{
    "baseUrl": ".",
        "appDir": "../control/src",
        "mainConfigFile": "../control/config.js",
        "findNestedDependencies": true,

        "optimiseCss": "standard",

        "dir": "../www-built",
        // List the modules that will be optimized.
        "modules": [
            {
                // module names are relative to baseUrl
                "name": "meefax"
            }
        ]
}
