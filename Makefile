.PHONY: control test

control:
	node tools/r.js -o tools/build.js
	tools/copy-in.sh

test:
	cd control; mocha
