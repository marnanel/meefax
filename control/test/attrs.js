var assert = require('assert');
var Attrs = require('../src/attrs.js');

function run_to_next_line(a) {
    start_classes = a.classes;
    for (var i=a.column; i<39; i++) {
        var found = a.handle(0x30);
        assert.equal(found, false);
        assert.equal(a.column, i+1);
        assert.equal(a.classes, start_classes);
    }
    a.handle(0x30);
}

describe('Attrs', function () {

    it('begins in the default state', function () {
        a = new Attrs();

        assert.equal(
            a.classes,
            'f7 b0'
        );
    });

    it('can change fg colour', function () {
        a = new Attrs();

        assert.equal(
            a.classes,
            'f7 b0'
        );

        a.handle(0x01);

        assert.equal(
            a.classes,
            'f1 b0'
        );
    });

    it('can change bg colour', function () {
        a = new Attrs();

        assert.equal(
            a.classes,
            'f7 b0'
        );

        a.handle(0x01);
        assert.equal(
            a.classes,
            'f1 b0'
        );

        a.handle(0x1d);
        assert.equal(
            a.classes,
            'f1 b1'
        );

        a.handle(0x06);
        assert.equal(
            a.classes,
            'f6 b1'
        );

        a.handle(0x1c);
        assert.equal(
            a.classes,
            'f6 b0'
        );
    });

    it('counts columns and resets at the end of the line', function () {
        a = new Attrs();
        assert.equal(a.column, 0);
        assert.equal(a.row, 0);
        assert.equal(a.classes, 'f7 b0');

        a.handle(0x02);
        assert.equal(a.column, 1);
        assert.equal(a.row, 0);
        assert.equal(a.classes, 'f2 b0');

        run_to_next_line(a);

        assert.equal(a.column, 0);
        assert.equal(a.row, 1);
        assert.equal(a.classes, 'f7 b0');
    });

    it('only returns True if the state changes', function() {
        a = new Attrs();
        assert.equal(a.handle(0x02), true);
        assert.equal(a.handle(0x02), false);
        assert.equal(a.handle(0x03), true);
        assert.equal(a.handle(0x03), false);
        assert.equal(a.handle(0x02), true);
    });

    it('alternates classes on double height lines', function() {

        a = new Attrs();

        for (var i=0; i<7; i++) {

            /*
             * 0: d1 (double height, top)
             * 1: d2 (double height, bottom)
             * 2: d1
             * 3: d2
             * 4: neither
             * 5: d1
             * 6: d2
             */

            var dh_classes;

            if (i==0 || i==2 || i==5) {
                dh_classes = 'f7 b0 d1';
            } else if (i==1 || i==3 || i==6) {
                dh_classes = 'f7 b0 d2';
            } else {
                dh_classes = 'f7 b0';
            }

            a.handle(0x30);
            assert.equal(a.classes, 'f7 b0');

            if (i==4) {
                a.handle(0x20);
            } else {
                a.handle(0x0d);
            }
            assert.equal(a.classes, dh_classes);

            a.handle(0x0c);
            assert.equal(a.classes, 'f7 b0');

            run_to_next_line(a);
        }
    });
});
