var assert = require('assert');
var Screen = require('../src/screen.js');

describe('Screen', function () {

    it('converts text with no control codes', function () {
        s = new Screen(
            // 23456789012345678901234567890123456789
            'Hello world.                            ' +
            'I love you!')

        assert.equal(
            s.contents,
            '<span class="f7 b0">Hello world.                            \n'+
            'I love you!</span>')

    });

    it('changes foreground colour', function () {
        s = new Screen(
            'What is blue?\x04This!');

        assert.equal(
            s.contents,
            '<span class="f7 b0">What is blue?</span>'+
            '<span class="f4 b0"> This!</span>');
    });

    it('does double height', function () {
        s = new Screen(
            'Small\x0dLarge\x0c'+
            "Small again                 Small"+
            '\x0dLarge\x0cSecond line'
        );

        assert.equal(
            s.contents,
            '<span class="f7 b0">Small</span>'+
            '<span class="f7 b0 d1"> Large</span>'+
            '<span class="f7 b0"> Small again                 \n'+
            "Small</span>"+
            '<span class="f7 b0 d2"> Large</span>'+
            '<span class="f7 b0"> Second line</span>');
    });

    it('can be tested with ascii', function() {
        s = Screen.from_ascii_for_testing(
            // 23456789012345678901234567890123456789
            " TDCEEFAX\n"+
            "^Y[\n"+
            " TDCEEFAX\n"+
            "^Y[\n"+
            " 0123456789012345678901234567890123456789\n"+
            " TNEW-LOOK BBC1 MAGAZINE: TMany regular\n"+
            "^C                        W\n"+
            " Tpages now have new numbers. This is the\n"+
            "^W\n"+
            " Tbasic pattern of the magazine:-\n"+
            "^W\n"+
            "\n"+
            " T101 TNEWS HEADLINES            NEW\n"+
            "^W    C\n"+
            " T101-119  TNews in detail       FULL\n"+
            "^W         C\n"+
            " T120 TFINANCE HEADLINES         INDEX\n"+
            "^W    C\n"+
            " T130 TFT INDEX\n"+
            "^W    C\n"+
            " T140 TSPORT HEADLINES          TA-FT193\n"+
            "^W    C                         C\n"+
            " T161 TFOOD GUIDE               TG-OT194\n"+
            "^W    C                         C   W\n"+
            " T180 TWEATHER & TRAVEL INDEX   TP-ZT195\n"+
            "^W    C                         C   W\n"+
            " T181 TWEATHER FORECAST\n"+
            "^W    C\n"+
            " T171-173 TTV GUIDE\n"+
            "^W        C\n"+
            " T    NEWSFLASHT150  TLATEST PAGEST190\n"+
            "^C             W     C            W\n"
                    );

        console.log(s);

        assert.equal(
            s.contents,
            '<span class="f7 b0"></span><span class="f3 b0">  CEEFAX' +
            '                                \n' +
            '</span><span class="f3 b0">  CEEFAX'+
            '                                \n' +
            '0123456789012345678901234567890123456789\n' +
            '</span><span class="f6 b0"> NEW-LOOK BBC1 MAGAZINE: </span><span class="f7 b0"> Many regular  \n' +
            ' pages now have new numbers. This is the\n' +
            ' basic pattern of the magazine:-        \n' +
            ' 101 </span><span class="f6 b0"> NEWS HEADLINES            NEW     \n' +
            ' 101-119  </span><span class="f6 b0"> News in detail       FULL    \n' +
            ' 120 </span><span class="f6 b0"> FINANCE HEADLINES         INDEX   \n' +
            ' 130 </span><span class="f6 b0"> FT INDEX                          \n' +
            ' 140 </span><span class="f6 b0"> SPORT HEADLINES           A-FT193 \n' +
            ' 161 </span><span class="f6 b0"> FOOD GUIDE                G-O</span><span class="f7 b0"> 194 \n' +
            ' 180 </span><span class="f6 b0"> WEATHER & TRAVEL INDEX    P-Z</span><span class="f7 b0"> 195 \n' +
            ' 181 </span><span class="f6 b0"> WEATHER FORECAST                  \n' +
            ' 171-173 </span><span class="f6 b0"> TV GUIDE                      \n' +
            '</span><span class="f6 b0">     NEWSFLASH</span><span class="f7 b0"> '+
            '150  </span><span class="f6 b0"> LATEST PAGES</span><span class="f7 b0"> 190   \n' +
            '</span>');
    });
});
/*
const GFX  = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
const TEXT = '';

describe('graphics', function () {

    it('turns text into graphics', function () {

        assert.equal(
            text_to_gfx(TEXT), GFX
            );
    });

}); 
*/
