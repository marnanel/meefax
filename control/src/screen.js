if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

var Attrs = require('../src/attrs.js');

define([], function() {
    function Screen(b) {

        console.log("constructor:", b);
        this.attrs = new Attrs();

        this.contents = '<span class="'+this.attrs.classes+'">';

        var x = 0;
        var y = 0;

        var current_line_dh_top = false;
        var previous_line_dh_top = false;

        for (var i=0, c=''; c = b.charAt(i); i++) {

            var code = c.charCodeAt(0);

            var change_span = this.attrs.handle(code);

            if (change_span) {
                this.contents += '</span><span class="';
                this.contents += this.attrs.classes+'">';
            }

            var normalised = this.attrs.normalise_control(code);

            if (normalised==null) {
                // not a control
                this.contents += c;
            } else {
                this.contents += ' ';
                // TODO "display controls" mode
                // TODO hold mode
            }

            if (this.attrs.column==0) {
                this.contents += '\n';
            }

        }

        this.contents += '</span>';

    };

    Screen.prototype.text_to_gfx = function(s) {

        var result = '';

        var target;

        for (var i=0, c=0, target=0; target = c = s.charCodeAt(i); i++) {

            if (c>=160 && c<=191) {
                target += 0xEE00;
            } else if (c>=224 && c<=255) {
                target += 0xEE40;
            }
            result += String.fromCharCode(target);
        }

        return result;
    }

    Screen.from_ascii_for_testing = function(s) {

        var CONTROLS = {
            '00': '\x00',

            'TR': '\x01',
            'TG': '\x02',
            'TY': '\x03',
            'TB': '\x04',
            'TM': '\x05',
            'TC': '\x06',
            'TW': '\x07',

            'F[': '\x08',
            'F]': '\x09',

            'X[': '\x0a',
            'X]': '\x0b',

            'D[': '\x0c',
            'D]': '\x0d',

            'SO': '\x0e',
            'SI': '\x0f',

            'DL': '\x10',

            'GR': '\x11',
            'GG': '\x12',
            'GY': '\x13',
            'GB': '\x14',
            'GM': '\x15',
            'GC': '\x16',
            'GY': '\x17',

            'R[': '\x18',

            'S[': '\x19',
            'S]': '\x1a',

            'ES': '\x1b',

            'B[': '\x1c',
            'B]': '\x1d',

            'H[': '\x1e',
            'H]': '\x1f',
        };

        var input_lines = [];
        var current_line = '';
        var start_of_line = true;
        var control_line = false;

        console.log("faft: begins");
        for (var i=0, c=''; c = s.charAt(i); i++) {
            console.log("faft: seen ", c);

            if (c=='\n') {
                console.log("faft: push at end of line");

                if (!control_line) {
                    while (current_line.length<40) {
                        current_line += ' ';
                    }

                    input_lines.push(current_line);
                }
                current_line = '';
                start_of_line = true;
            } else if (start_of_line) {
                if (c==' ') {
                    control_line = false;
                } else if (c=='^') {
                    control_line = true;
                } else {
                    console.log("Error: lines must begin with caret or space");
                    return null;
                }
                start_of_line = false;
            } else if (control_line) {
                if (c!=' ') {
                    var last_line = input_lines[input_lines.length-1];
                    var above = last_line[current_line.length];
                    var sequence = above+c;
                    console.log("faft: control:", sequence);

                    var control_char = CONTROLS[sequence];

                    if (control_char==undefined) {
                        console.log("faft: error: no such control sequence",
                            sequence);
                        return null
                    }
                    console.log("faft: represents:", control_char);

                    console.log(last_line);
                    var chars = last_line.split('');
                    chars[current_line.length] = control_char;

                    input_lines[input_lines.length-1] = chars.join('');
                    console.log(input_lines[input_lines.length-1]);

                }
                current_line += c;
            } else {
                current_line += c;
            }

            if (current_line.length>40) {
                console.log("faft: error: line is too long!: ", current_line);
                return null;
            }
        }
        input_lines.push(current_line);

        if (input_lines.length>25) {
            console.log("faft: error: too many lines!", input_lines);
            return null;
        }

        console.log("faft: result is:", input_lines)
        console.log("faft: done");

        var blob = input_lines.join('');
        console.log("9111", blob);
        console.log("9112", blob.length);

        var result = new Screen(blob);
        console.log("9119", result);
        return result;

    }

    if (module!==undefined) {
        module.exports = Screen;
    }

    return Screen;

});
