if (typeof define !== 'function') {
    var define = require('amdefine')(module);
}

define([], function() {

    function Attrs() {
        this.has_flash = false;
        this.has_boxes = false;
        this.dh_previous_line = false;
        this.dh_this_line = false;
        this.row = -1;

        this.newline();
    }

    Attrs.prototype.BLACK = 0;
    Attrs.prototype.RED = 1;
    Attrs.prototype.GREEN = 2;
    Attrs.prototype.YELLOW = 3;
    Attrs.prototype.BLUE = 4;
    Attrs.prototype.MAGENTA = 5;
    Attrs.prototype.CYAN = 6;
    Attrs.prototype.WHITE = 7;

    Attrs.prototype.newline = function newline() {
        this.fg = this.WHITE;
        this.bg = this.BLACK;

        this.dh_previous_line = this.dh_this_line ^ this.dh_previous_line;

        this.graphics = false;
        this.flash = false;
        this.dh = false;
        this.dh_this_line = false;
        this.boxed = false;
        this.conceal = false;

        this.column = 0;
        this.row += 1;

        this._generate_classes();
    }

    Attrs.prototype._generate_classes = function() {
        this.classes = 'f'+this.fg+' b'+this.bg;

        if (this.flash) { this.classes += ' flash'; }
        if (this.dh) {
            if (this.dh_previous_line) {
                this.classes += ' d2';
            } else {
                this.classes += ' d1';
            }
        }
        if (this.sep) { this.classes += ' sep'; }
        if (this.boxed) { this.classes += ' boxed'; }

        // "graphics", "conceal", and "hold" don't affect CSS classes
    }

    Attrs.prototype.handle = function(c) {

        this.column += 1;
        if (this.column > 39) {
            this.newline();
        }

        c = this.normalise_control(c);

        if (c==null) {
            return false;
        }

        var classes_before = this.classes;

        switch (c) {
            case 0x00: break; // NUL

            case 0x01: this.fg = this.RED;     this.graphics=false;   break;
            case 0x02: this.fg = this.GREEN;   this.graphics=false;   break;
            case 0x03: this.fg = this.YELLOW;  this.graphics=false;   break;
            case 0x04: this.fg = this.BLUE;    this.graphics=false;   break;
            case 0x05: this.fg = this.MAGENTA; this.graphics=false;   break;
            case 0x06: this.fg = this.CYAN;    this.graphics=false;   break;
            case 0x07: this.fg = this.WHITE;   this.graphics=false;   break;

            case 0x08: this.flash = true; break;
            case 0x09: this.flash = false; break;

            case 0x0a: this.boxed = false;                            break;
            case 0x0b: this.boxed = true;  this.has_boxes = true;     break;

            case 0x0c: this.dh = false;                               break;
            case 0x0d: this.dh = true; this.dh_this_line = true;      break;

            case 0x0e: break; // SO
            case 0x0f: break; // SI

            case 0x10: break; // DLE

            case 0x11: this.fg = this.RED;     this.graphics=true;    break;
            case 0x12: this.fg = this.GREEN;   this.graphics=true;    break;
            case 0x13: this.fg = this.YELLOW;  this.graphics=true;    break;
            case 0x14: this.fg = this.BLUE;    this.graphics=true;    break;
            case 0x15: this.fg = this.MAGENTA; this.graphics=true;    break;
            case 0x16: this.fg = this.CYAN;    this.graphics=true;    break;
            case 0x17: this.fg = this.WHITE;   this.graphics=true;    break;

            case 0x18: this.conceal = true; this.has_conceal = true;  break;

            case 0x19: this.sep = false;                              break;
            case 0x1a: this.sep = true;                               break;

            case 0x1b: break; // ESC

            case 0x1c: this.bg = this.BLACK;                          break;
            case 0x1d: this.bg = this.fg;                             break;

            case 0x1e: this.hold = true;                              break;
            case 0x1f: this.hold = false;                             break;
        }

        this._generate_classes();

        return classes_before!==this.classes;
    }

    Attrs.prototype.normalise_control = function(c) {
        if (c>=0x0001 && c<=0x001F) {
            // which is as used in the original spec--
            // good, that's already normalised
            return c;
        } else if (c>=0x0081 && c<=0x009F) {
            // which is as used on the BBC Micro
            return c & 0x1F;
        } else if (c>=0xEF01 && c<=0xEF1F) {
            // which is where we're putting the symbols
            // so they don't confuse modern editors
            return c & 0x1F;
        } else {
            // not our problem
            return null;
        }
    }

    if (module!==undefined) {
        module.exports = Attrs;
    }

    return Attrs;

});
